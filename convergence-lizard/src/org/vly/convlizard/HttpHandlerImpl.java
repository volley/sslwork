package org.vly.convlizard;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;

class HttpHandlerImpl implements HttpHandler {

  private static final AtomicInteger atomic = new AtomicInteger();

  private final Semaphore semaphore = new Semaphore(0);
  private final String id;

  HttpHandlerImpl(String id) {
    this.id = id;
  }

  void waitUntilOneRequestHandled() {
    semaphore.acquireUninterruptibly();
  }

  @Override
  public void handle(HttpExchange httpExchange) throws IOException {
    PunyLog.log("Serving " + httpExchange.getRemoteAddress());

    final byte[] body = createBody(httpExchange);

    httpExchange.sendResponseHeaders(200, body.length);
    httpExchange.getResponseBody().write(body, 0, body.length);
    httpExchange.close();

    semaphore.release();
  }

  private byte[] createBody(HttpExchange httpExchange) {
    final int n = atomic.incrementAndGet();
    final ByteArrayOutputStream baos = new ByteArrayOutputStream();
    final PrintWriter printWriter = new PrintWriter(baos);
    printWriter.println("<body>");
    printWriter.println("<p>" + httpExchange.getRemoteAddress() + ", visitor #" + n + ", served by " + id + ".</p>");
    printWriter.println("<p>If you can read this message you are not protected by <a href=\"http://convergence.io\">Convergence</a>.</p>");
    printWriter.println("</body>");
    printWriter.close();
    return baos.toByteArray();
  }
}
