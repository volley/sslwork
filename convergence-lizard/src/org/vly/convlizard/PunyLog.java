package org.vly.convlizard;

import java.util.Date;

class PunyLog {

  private static String getTime() {
    return new Date().toString();
  }

  public static void log(String message) {
    System.out.println(getTime() + " Lizard: " + message);
  }

  public static void log(String message, Throwable t) {
    System.out.println(getTime() + " Lizard: " + message);
    t.printStackTrace(System.out);
  }

}
