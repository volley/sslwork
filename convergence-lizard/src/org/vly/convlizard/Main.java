package org.vly.convlizard;

public class Main {

  public static void main(String[] args) {
    int numberOfUniqueCerts = 128;

    if (args.length > 0) {
      numberOfUniqueCerts = Integer.parseInt(args[0]);
    }

    final LizardServer lizardServer = new LizardServer(numberOfUniqueCerts);

    lizardServer.runLaps();
  }

}
