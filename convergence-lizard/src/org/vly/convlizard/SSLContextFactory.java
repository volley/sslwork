package org.vly.convlizard;

import sun.security.tools.keytool.CertAndKeyGen;
import sun.security.x509.X500Name;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import java.security.KeyStore;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.GregorianCalendar;

class SSLContextFactory {

  private static final Date time = new GregorianCalendar().getTime();
  private static final String nameString = "C=SE+O=org.vly";
  private static final long validity = 1024L * 24L * 60L * 60L;
  private static final char[] password = "brainz".toCharArray();

  /**
   * The certs created by this function share as many attributes as possible
   * (including date, validity and name). Only the actual key varies.
   */
  SSLContext createSSLContext() throws Exception {
    final CertAndKeyGen keyPair = new CertAndKeyGen("DSA", "SHA1WithDSA", null);
    keyPair.generate(1024);

    final X500Name name = new X500Name(nameString);
    final X509Certificate certificate = keyPair.getSelfCertificate(name, time, validity);
    final X509Certificate[] chain = new X509Certificate[]{certificate};

    final KeyStore keyStore = KeyStore.getInstance("JKS");
    keyStore.load(null, password);
    keyStore.setKeyEntry("alias", keyPair.getPrivateKey(), password, chain);

    final KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
    kmf.init(keyStore, password);

    final TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
    tmf.init(keyStore);

    final SSLContext sslContext = SSLContext.getInstance("TLS");
    sslContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
    return sslContext;
  }

}
