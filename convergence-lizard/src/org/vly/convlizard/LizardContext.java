package org.vly.convlizard;

import javax.net.ssl.SSLContext;

class LizardContext {
  final int i;
  final SSLContext sslContext;

  LizardContext(int i, SSLContext sslContext) {
    this.i = i;
    this.sslContext = sslContext;
  }

  @Override
  public String toString() {
    return String.valueOf(i);
  }
}
