package org.vly.convlizard;

import com.sun.net.httpserver.HttpsConfigurator;
import com.sun.net.httpserver.HttpsParameters;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.SSLParameters;

class HttpsConfiguratorImpl extends HttpsConfigurator {
  public HttpsConfiguratorImpl(SSLContext sslContext) {
    super(sslContext);
  }

  @Override
  public void configure(HttpsParameters params) {
    PunyLog.log("Accepted " + params.getClientAddress());
    try {
      final SSLEngine engine = getSSLContext().createSSLEngine();
      params.setNeedClientAuth(false);
      params.setCipherSuites(engine.getEnabledCipherSuites());
      params.setProtocols(engine.getEnabledProtocols());

      final SSLParameters defaultParams = getSSLContext().getDefaultSSLParameters();
      params.setSSLParameters(defaultParams);
    } catch (Exception ex) {
      PunyLog.log("Configuration failed", ex);
    }
  }
}
