package org.vly.convlizard;

import com.sun.net.httpserver.HttpsServer;

import java.net.InetSocketAddress;

class LizardServer {

  private final SSLContextFactory sslContextFactory = new SSLContextFactory();

  private final LizardContext[] lizardContexts;

  LizardServer(int numberOfUniqueCerts) {
    this.lizardContexts = new LizardContext[numberOfUniqueCerts];
  }

  void runLaps() {
    int lapCount = 0;
    //noinspection InfiniteLoopStatement
    while (true) {
      lapCount++;
      PunyLog.log("Start of lap " + lapCount);
      runLap();
    }
  }

  private void runLap() {
    for (int contextIndex = 0; contextIndex < lizardContexts.length; contextIndex++) {
      try {
        if (lizardContexts[contextIndex] == null) {
          lizardContexts[contextIndex] = new LizardContext(contextIndex, sslContextFactory.createSSLContext());
        }
        serveOne(lizardContexts[contextIndex]);
      } catch (Exception exception) {
        PunyLog.log("Failed to serve", exception);
        shortSleepToAvoidCpuHog();
      }
    }
  }

  private static void shortSleepToAvoidCpuHog() {
    try {
      Thread.sleep(250);
    } catch (InterruptedException e) {
      Thread.currentThread().interrupt();
    }
  }

  private void serveOne(LizardContext lizardContext) throws Exception {
    final HttpHandlerImpl httpHandlerImpl = new HttpHandlerImpl("SSL context " + lizardContext.i);

    final HttpsServer httpsServer = HttpsServer.create(new InetSocketAddress(443), 0);
    httpsServer.createContext("/", httpHandlerImpl);
    httpsServer.setHttpsConfigurator(new HttpsConfiguratorImpl(lizardContext.sslContext));

    httpsServer.start();
    PunyLog.log("Server " + lizardContext.i + " up");

    httpHandlerImpl.waitUntilOneRequestHandled();

    // A client sending multiple requests over the same session will be annoyed by this.
    httpsServer.stop(0);
    PunyLog.log("Server " + lizardContext.i + " down");
  }

}
